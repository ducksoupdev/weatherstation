(function () {
    "use strict";

    describe("Viewing weather", function() {
        beforeEach(function() {
            browser().navigateTo("/index.html");
        });

        it("Should load the cities", function() {
            expect(element(".city").count()).toEqual(4);
        });

        it("Should display a generic icon for each city", function() {
            expect(element("#wrapper div:nth-of-type(2) .head .icon .generic").count()).toBe(1);
            expect(element("#wrapper div:nth-of-type(2) .head .icon .generic").count()).toBe(1);
            expect(element("#wrapper div:nth-of-type(2) .head .icon .generic").count()).toBe(1);
            expect(element("#wrapper div:nth-of-type(2) .head .icon .generic").count()).toBe(1);
        });

        it("Should display a sort by temperature link", function() {
            expect(element("#wrapper .sort a").count()).toBe(1);
            expect(element("#wrapper .sort a span:nth-of-type(1)").html()).toEqual("Sort by temperature");
            expect(element("#wrapper .sort a span:nth-of-type(2)").attr("class")).toContain("ascending");
        });

        describe("Clicking the first weather", function() {
            beforeEach(function() {
                element("#wrapper div:nth-of-type(2) .head a").click();
            });

            it("Should display a weather-specific icon", function() {
                expect(element("#wrapper div:nth-of-type(2) .head .icon .weather").count()).toBe(1);
            });

            it("Should display the temperature", function() {
                expect(element("#wrapper div:nth-of-type(2) .head .icon .temperature").count()).toBe(1);
                expect(element("#wrapper div:nth-of-type(2) .head .icon .temperature").html()).toEqual("281.06");
            });

            it("Should display the body section", function() {
                expect(element("#wrapper div:nth-of-type(2) .body").count()).toBe(1);
            });

            it("Should display a body weather section", function() {
                expect(element("#wrapper div:nth-of-type(2) .body div:nth-of-type(1) .title").html()).toEqual("Weather");
            });

            it("Should display the correct weather", function() {
                expect(element("#wrapper div:nth-of-type(2) .body div:nth-of-type(1) div span").html()).toEqual("Clouds");
            });

            it("Should display a body location section", function() {
                expect(element("#wrapper div:nth-of-type(2) .body div:nth-of-type(2) .title").html()).toEqual("Location");
            });

            it("Should display the correct location", function() {
                expect(element("#wrapper div:nth-of-type(2) .body div:nth-of-type(2) div:nth-of-type(1) span:nth-of-type(2)").html()).toEqual("-0.13");
                expect(element("#wrapper div:nth-of-type(2) .body div:nth-of-type(2) div:nth-of-type(2) span:nth-of-type(2)").html()).toEqual("51.51");
            });

            it("Should display a body temperature section", function() {
                expect(element("#wrapper div:nth-of-type(2) .body div:nth-of-type(3) .title").html()).toEqual("Temperature");
            });

            it("Should display the correct temperature", function() {
                expect(element("#wrapper div:nth-of-type(2) .body div:nth-of-type(3) div:nth-of-type(1) span:nth-of-type(2)").html()).toEqual("282.15");
                expect(element("#wrapper div:nth-of-type(2) .body div:nth-of-type(3) div:nth-of-type(2) span:nth-of-type(2)").html()).toEqual("279.15");
            });

            it("Should display a body pressure section", function() {
                expect(element("#wrapper div:nth-of-type(2) .body div:nth-of-type(4) .title").html()).toEqual("Pressure");
            });

            it("Should display the correct pressure", function() {
                expect(element("#wrapper div:nth-of-type(2) .body div:nth-of-type(4) div span").html()).toEqual("1004");
            });

            it("Should display a body humidity section", function() {
                expect(element("#wrapper div:nth-of-type(2) .body div:nth-of-type(5) .title").html()).toEqual("Humidity");
            });

            it("Should display the correct humidity", function() {
                expect(element("#wrapper div:nth-of-type(2) .body div:nth-of-type(5) div span").html()).toEqual("93");
            });

            describe("Clicking the next weather", function() {
                beforeEach(function() {
                    element("#wrapper div:nth-of-type(3) .head a").click();
                });

                it("Should collapse the first weather", function() {
                    expect(element("#wrapper div:nth-of-type(2) .body").count()).toBe(0);
                });

                it("Should display the body section of the next weather", function() {
                    expect(element("#wrapper div:nth-of-type(3) .body").count()).toBe(1);
                });
            });
        });
    });
})();
