(function () {
    "use strict";

    describe("weatherService", function() {
        var weatherService;
        var testWeatherUri = "http://localhost/weather?q=London,uk&APPID=test123";
        var mockRuntimeService = jasmine.createSpyObj("runtimeService", ["useLiveData"]);
        var $httpBackend;

        beforeEach(module("weatherStationApp"));

        beforeEach(module(function($provide) {
            $provide.value("runtimeService", mockRuntimeService);
            var serviceUris = {
                weatherUri: testWeatherUri
            };
            $provide.value("serviceUris", serviceUris);
        }));

        beforeEach(inject(function($injector) {
            $httpBackend = $injector.get("$httpBackend");
            weatherService = $injector.get("weatherService");
        }));

        beforeEach(function() {
            this.addMatchers({
                toEqualData: function(expect) {
                    return angular.equals(expect, this.actual);
                }
            });
        });

        describe("When in http mode", function() {
            var cityName = "London";

            beforeEach(function() {
                mockRuntimeService.useLiveData.andCallFake(function() {
                    return true;
                });
            });

            it("should return a result from the OpenWeatherMap server", function() {
                $httpBackend.expectGET(testWeatherUri).respond(function() {
                    return [200, {
                        weather: {
                            "id": 803,
                            "main": "Clouds",
                            "description": "broken clouds",
                            "icon": "04n"
                        }
                    }, {}];
                });
                weatherService.load(cityName).then(function(response) {
                    expect(response).toEqualData({
                        weather: {
                            "id": 803,
                            "main": "Clouds",
                            "description": "broken clouds",
                            "icon": "04n"
                        }
                    });
                });
            });

            it("should handle an error from the server", function() {
                $httpBackend.expectGET(testWeatherUri).respond(function() {
                    return [500, {}, {}]
                });
                weatherService.load(cityName).then(function(response) {
                    // ignored because this doesn't get called
                }, function(response) {
                    expect(response.status).toBe(500);
                });
            });
        });

        describe("When in file mode", function() {
            var cityName = "London";

            beforeEach(function() {
                mockRuntimeService.useLiveData.andCallFake(function() {
                    return false;
                });
            });

            it("should return a '200' result from a local resource", function() {
                weatherService.load(cityName).then(function(response) {
                    expect(response.status).toEqualData("200");
                });
            });
        });
    });
})();
