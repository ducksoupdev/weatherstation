(function () {
    "use strict";

    describe("weatherController", function() {
        var weatherController, $q, weatherService, $rootScope;
        var weatherData = {
            "id": 803,
            "main": "Clouds",
            "description": "broken clouds",
            "icon": "04n",
            weather: [
                {
                    main: "Test"
                }
            ]
        };

        beforeEach(module("weatherStationApp"));

        var mockWeatherService = jasmine.createSpyObj("weatherService", ["load"]);
        mockWeatherService.load.andCallFake(function() {
            return questions;
        });

        beforeEach(inject(function(_$controller_, _$q_, _weatherService_, _$rootScope_) {
            $q = _$q_;
            $rootScope = _$rootScope_;
            weatherService = _weatherService_;
            weatherController = _$controller_("weatherController", {});

            spyOn(weatherService, "load").andCallFake(function(results) {
                var deferred = $q.defer();
                deferred.resolve(weatherData);
                return deferred.promise;
            });
        }));

        describe("The loadWeather method", function() {
            var city;
            beforeEach(function() {
                city = {
                    name: "London"
                };
                weatherController.loadWeather(city);
                $rootScope.$digest(); // needed for the promise to be called
            });

            it("Should call the weather service load method", function() {
                expect(weatherService.load).toHaveBeenCalled();
            });

            it("Should populate the weather data", function() {
                expect(city.weather).toBeDefined();
                expect(city.weather.data).toEqual(weatherData);
            });

            it("Should set the active city", function() {
                expect(weatherController.state.active).toEqual(city);
            });
        });

        describe("The sortByTemperature method", function() {
            it("Should be ascending by default", function() {
                expect(weatherController.state.sortProperty).toEqual("+weather.temperature");
            });

            describe("Setting the sort order", function() {
                beforeEach(function() {
                    weatherController.sortByTemperature();
                });

                it("Should set the sort to descending", function() {
                    expect(weatherController.state.sortProperty).toEqual("-weather.temperature");
                });

                describe("Setting it back to ascending", function() {
                    beforeEach(function() {
                        weatherController.sortByTemperature();
                    });

                    it("Should set the sort to ascending", function() {
                        expect(weatherController.state.sortProperty).toEqual("+weather.temperature");
                    });
                });
            });
        });
    });
})();
