(function () {
    "use strict";

    describe("arraySearch", function () {
        beforeEach(module("weatherStationApp"));

        var arraySearch = null;

        beforeEach(inject(function($injector){
            arraySearch = $injector.get("arraySearch");
        }));

        var testArray = null;

        beforeEach(function() {
            testArray = [
                {
                    Id: 1,
                    name: "Cheese"
                },
                {
                    Id: 2,
                    name: "Ham"
                },
                {
                    Id: 3,
                    name: "Onions"
                },
                {
                    Id: 4,
                    name: "Ham"
                }
            ];
        });

        describe("when searching for a single match", function() {
            it("Should return null if the array to search is null", function() {
                var item = arraySearch.single(null, function(item) {
                    return item.name === "Ham";
                });
                expect(item).toBeNull();
            });

            it("returns the first item that matches", function() {
                var item = arraySearch.single(testArray, function(item) {
                    return item.name == "Ham";
                });
                expect(item).toBe(testArray[1]);
            });

            it( "returns null if no match found", function() {
                var item = arraySearch.single(testArray, function(item) {
                    return item.name == "Hample";
                });
                expect(item).toBe(null);
            });
        });
    });
})();
