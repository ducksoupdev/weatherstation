(function () {
    "use strict";

    describe("Viewing weather", function() {
        beforeEach(function() {
            browser().navigateTo("/index.html");
        });

        it("Should load the cities", function() {
            expect(element(".city").count()).toEqual(4);
        });

        it("Should display a generic icon for each city", function() {
            expect(element("#wrapper div:nth-of-type(2) .head .icon .generic").count()).toBe(1);
            expect(element("#wrapper div:nth-of-type(2) .head .icon .generic").count()).toBe(1);
            expect(element("#wrapper div:nth-of-type(2) .head .icon .generic").count()).toBe(1);
            expect(element("#wrapper div:nth-of-type(2) .head .icon .generic").count()).toBe(1);
        });

        it("Should display a sort by temperature link", function() {
            expect(element("#wrapper .sort a").count()).toBe(1);
            expect(element("#wrapper .sort a span:nth-of-type(1)").html()).toEqual("Sort by temperature");
            expect(element("#wrapper .sort a span:nth-of-type(2)").attr("class")).toContain("ascending");
        });

        describe("Clicking the first weather", function() {
            beforeEach(function() {
                element("#wrapper div:nth-of-type(2) .head a").click();
            });

            it("Should display a weather-specific icon", function() {
                expect(element("#wrapper div:nth-of-type(2) .head .icon .weather").count()).toBe(1);
            });

            it("Should display the temperature", function() {
                expect(element("#wrapper div:nth-of-type(2) .head .icon .temperature").count()).toBe(1);
            });

            it("Should display the body section", function() {
                expect(element("#wrapper div:nth-of-type(2) .body").count()).toBe(1);
            });

            it("Should display a body weather section", function() {
                expect(element("#wrapper div:nth-of-type(2) .body div:nth-of-type(1) .title").html()).toEqual("Weather");
            });

            it("Should display a body location section", function() {
                expect(element("#wrapper div:nth-of-type(2) .body div:nth-of-type(2) .title").html()).toEqual("Location");
            });

            it("Should display a body temperature section", function() {
                expect(element("#wrapper div:nth-of-type(2) .body div:nth-of-type(3) .title").html()).toEqual("Temperature");
            });

            it("Should display a body pressure section", function() {
                expect(element("#wrapper div:nth-of-type(2) .body div:nth-of-type(4) .title").html()).toEqual("Pressure");
            });

            it("Should display a body humidity section", function() {
                expect(element("#wrapper div:nth-of-type(2) .body div:nth-of-type(5) .title").html()).toEqual("Humidity");
            });

            describe("Clicking the next weather", function() {
                beforeEach(function() {
                    element("#wrapper div:nth-of-type(3) .head a").click();
                });

                it("Should collapse the first weather", function() {
                    expect(element("#wrapper div:nth-of-type(2) .body").count()).toBe(0);
                });

                it("Should display the body section of the next weather", function() {
                    expect(element("#wrapper div:nth-of-type(3) .body").count()).toBe(1);
                });
            });
        });
    });
})();
