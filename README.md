#Weather station test app

_Version 0.1.0_

##Requirements

The following are required before installing the weather station app:

* [NodeJS](http://nodejs.org)
* [Gulp](http://gulpjs.com)
* [Bower](http://bower.io)

##Installation

Run the following to install the project dependencies:

`npm install`

##Tests

Unit tests, spec tests and e2e test can be run using the following commands:

`gulp jsunit`

`gulp spec`

`gulp e2e`

##Run the app

To view the app, run the following command:

`gulp server`
