(function() {
    "use strict";

    angular.module("weatherStationApp")
        .factory("arraySearch", arraySearch);

    function arraySearch() {
        return {
            single: function (array, compareFunc) {
                if (!array) {
                    return null;
                }
                for ( var index = 0; index < array.length; index++) {
                    var item = array[index];
                    if ( compareFunc(item) ) {
                        return item;
                    }
                }
                return null;
            }
        };
    }
})();