(function() {
    "use strict";

    angular.module("weatherStationApp")
        .factory("serviceUris", serviceUris);

    function serviceUris() {
        return {
            weatherUri: "http://api.openweathermap.org/data/2.5/weather"
        };
    }
})();
