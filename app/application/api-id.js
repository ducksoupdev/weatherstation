(function() {
    "use strict";

    angular.module("weatherStationApp")
        .value("apiIds", {
            weatherUri: "c7a517b75f9507698e5b843bd029f79b"
        });
})();