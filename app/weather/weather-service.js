(function() {
    "use strict";

    angular.module("weatherStationApp")
        .factory("weatherService", weatherService);

    weatherService.$inject = [
        "$resource",
        "$q",
        "runtimeService",
        "serviceUris",
        "apiIds",
        "testWeather"
    ];

    function weatherService(
        $resource,
        $q,
        runtimeService,
        serviceUris,
        apiIds,
        testWeather
    ) {
        return {
            load: function(cityName) {
                var deferred = $q.defer();

                if (runtimeService.useLiveData()) {
                    var weatherResource = $resource(serviceUris.weatherUri, {}, {
                        query: {
                            method: "GET",
                            isArray: false
                        }
                    });

                    weatherResource.query({ q: cityName + ",uk", APPID: apiIds.weatherUri, units: "metric" }).$promise.then(function(result) {
                        deferred.resolve(result);
                    }, function(response) {
                        deferred.reject(response);
                    });
                } else {
                    deferred.resolve(testWeather.load(cityName));
                }

                return deferred.promise;
            }
        };
    }
})();