(function() {
    "use strict";

    angular.module("weatherStationApp")
        .controller("weatherController", weatherController);

    weatherController.$inject = [
        "weatherService",
        "arraySearch"
    ];

    function weatherController(
        weatherService,
        arraySearch
    ) {
        var ctrl = this;

        ctrl.state = {
            active: null,
            progress: false,
            sortProperty: "+weather.temperature"
        };

        ctrl.cities = {
            list: [
                { name: "London" },
                { name: "Luton" },
                { name: "Manchester" },
                { name: "Birmingham" }
            ]
        };

        ctrl.loadWeather = function(city) {
            var now = new Date();
            var ten = 1000 * 60 * 60 * 10;

            // return if the data was fetched in the last 10 minutes
            if (angular.isDefined(city.weather) && ((now.getTime() - city.weather.ttl.getTime()) <= ten)) {
                ctrl.state.active = city;
                return;
            }

            // set the progress
            ctrl.state.progress = true;

            weatherService.load(city.name).then(function(result) {
                city.weather = {
                    data: result,
                    title: result.weather[0].main,
                    temperature: result.main.temp,
                    icon: "http://openweathermap.org/img/w/" + result.weather[0].icon + ".png",
                    ttl: new Date() // keep the date so we don't reload for another 10 minutes
                };

                // unset the progress
                ctrl.state.progress = false;
            }, function(res) {
                if (angular.isDefined(city.weather)) {
                    city.weather.lastError = res;
                } else {
                    city.weather = {
                        lastError: res
                    };
                }

                // unset the progress
                ctrl.state.progress = false;
            });

            ctrl.state.active = city;
        };

        ctrl.sortByTemperature = function() {
            if (ctrl.state.sortProperty === "+weather.temperature") {
                ctrl.state.sortProperty = "-weather.temperature";
            } else {
                ctrl.state.sortProperty = "+weather.temperature";
            }
        };
    }
})();