(function () {
    "use strict";

    angular.module("weatherStationApp")
        .factory("testWeather", testWeather);

    function testWeather() {
        return {
            load: function(cityName) {
                switch(cityName.toLowerCase()) {
                    case "london":
                        return {
                            "coord": {
                                "lon": -0.13,
                                "lat": 51.51
                            },
                            "sys": {
                                "type": 1,
                                "id": 5091,
                                "message": 0.0469,
                                "country": "GB",
                                "sunrise": 1416208963,
                                "sunset": 1416240503
                            },
                            "weather": [
                                {
                                    "id": 803,
                                    "main": "Clouds",
                                    "description": "broken clouds",
                                    "icon": "04n"
                                }
                            ],
                            "base": "cmc stations",
                            "main": {
                                "temp": 281.06,
                                "pressure": 1004,
                                "humidity": 93,
                                "temp_min": 279.15,
                                "temp_max": 282.15
                            },
                            "wind": {
                                "speed": 2.6,
                                "deg": 50
                            },
                            "clouds": {
                                "all": 75
                            },
                            "dt": 1416256337,
                            "id": 2643743,
                            "name": "London",
                            "cod": 200
                        };
                    case "luton":
                        return {
                            "coord": {
                                "lon": -0.42,
                                "lat": 51.88
                            },
                            "sys": {
                                "type": 1,
                                "id": 5078,
                                "message": 0.0316,
                                "country": "GB",
                                "sunrise": 1416209118,
                                "sunset": 1416240488
                            },
                            "weather": [
                                {
                                    "id": 802,
                                    "main": "Clouds",
                                    "description": "scattered clouds",
                                    "icon": "03n"
                                }
                            ],
                            "base": "cmc stations",
                            "main": {
                                "temp": 281.6,
                                "pressure": 1004,
                                "humidity": 100,
                                "temp_min": 280.15,
                                "temp_max": 282.15
                            },
                            "wind": {
                                "speed": 2.6,
                                "deg": 70
                            },
                            "clouds": {
                                "all": 40
                            },
                            "dt": 1416256337,
                            "id": 2643339,
                            "name": "Luton",
                            "cod": 200
                        };
                    case "manchester":
                        return {
                            "coord": {
                                "lon": -2.24,
                                "lat": 53.48
                            },
                            "sys": {
                                "type": 1,
                                "id": 5060,
                                "message": 0.2487,
                                "country": "GB",
                                "sunrise": 1416209943,
                                "sunset": 1416240536
                            },
                            "weather": [
                                {
                                    "id": 701,
                                    "main": "Mist",
                                    "description": "mist",
                                    "icon": "50n"
                                }
                            ],
                            "base": "cmc stations",
                            "main": {
                                "temp": 281.78,
                                "pressure": 1007,
                                "humidity": 87,
                                "temp_min": 281.15,
                                "temp_max": 282.35
                            },
                            "wind": {
                                "speed": 4.6,
                                "deg": 40
                            },
                            "clouds": {
                                "all": 20
                            },
                            "dt": 1416255567,
                            "id": 2643123,
                            "name": "Manchester",
                            "cod": 200
                        };
                    case "birmingham":
                        return {
                            "coord": {
                                "lon": -1.9,
                                "lat": 52.48
                            },
                            "sys": {
                                "type": 1,
                                "id": 5055,
                                "message": 0.588,
                                "country": "GB",
                                "sunrise": 1416209616,
                                "sunset": 1416240701
                            },
                            "weather": [
                                {
                                    "id": 801,
                                    "main": "Clouds",
                                    "description": "few clouds",
                                    "icon": "02n"
                                }
                            ],
                            "base": "cmc stations",
                            "main": {
                                "temp": 281.41,
                                "pressure": 1005,
                                "humidity": 100,
                                "temp_min": 280.15,
                                "temp_max": 282.55
                            },
                            "wind": {
                                "speed": 3.1,
                                "deg": 10
                            },
                            "clouds": {
                                "all": 20
                            },
                            "dt": 1416255390,
                            "id": 2655603,
                            "name": "Birmingham",
                            "cod": 200
                        }
                }
            }
        };
    };
})();
