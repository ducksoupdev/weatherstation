(function() {
    var gulp = require("gulp"),
        sass = require("gulp-sass"),
        connect = require("gulp-connect"),
        open = require("gulp-open"),
        concat = require("gulp-concat"),
        rename = require("gulp-rename"),
        minifyCss = require("gulp-minify-css"),
        uglify = require("gulp-uglify"),
        karma = require("karma").server;

    gulp.task("default", function() {

    });

    gulp.task("jsunit", function(done) {
        karma.start({
            configFile: __dirname + '/karma.conf.js',
            singleRun: true
        }, done);
    });

    gulp.task("spec", ["connect-test"], function() {
        return karma.start({
            configFile: __dirname + '/karma.spec.conf.js',
            singleRun: true
        });
    });

    gulp.task("e2e", ["connect"], function() {
        return karma.start({
            configFile: __dirname + '/karma.e2e.conf.js',
            singleRun: true
        });
    });

    gulp.task("sass", function() {
        gulp.src("./sass/*.scss")
            .pipe(sass())
            .pipe(gulp.dest("css"))
            .pipe(rename({suffix: ".min"}))
            .pipe(minifyCss())
            .pipe(gulp.dest("css"));
    });

    gulp.task("connect-test", function() {
        connect.server({
            root: ["."],
            port: 59001,
            livereload: true
        });
    });

    gulp.task("connect", function() {
        connect.server({
            root: ["."],
            port: 8888,
            livereload: true
        });
    });

    gulp.task("open", function() {
        gulp.src("./index.html")
            .pipe(open("http://localhost:8888"));
    });

    gulp.task("uglify", function() {
        return gulp.src("app/**/*.js")
            .pipe(concat("app.js"))
            .pipe(rename({suffix: '.min'}))
            .pipe(uglify())
            .pipe(gulp.dest("app"));
    });

    gulp.task("server", ["connect", "open"]);
})();