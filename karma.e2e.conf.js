(function () {
    "use strict";

    module.exports = function(config) {
        config.set({
            basePath: "",
            files: [
                "node_modules/karma-ng-scenario/lib/angular-scenario.js",
                "node_modules/karma-ng-scenario/lib/adapter.js",
                "test/e2e/*.spec.js",
                { pattern: "index.html", included: false, served: true },
                { pattern: "app/**/*.js", included: false, served: true },
                { pattern: "css/*.css", included: false, served: true },
                { pattern: "bower_components/**/*.js", included: false, served: true },
                { pattern: "bower_components/**/*.map", included: false, served: true },
                { pattern: "bower_components/**/*.css", included: false, served: true }
            ],
            reporters: ["progress"],
            loggers: [
                {
                    type: 'console'
                }
            ],
            port: 58500,
            runnerPort: 9200,
            colors: true,
            logLevel: config.LOG_INFO,
            autoWatch: false,
            browsers: ["PhantomJS"],
            captureTimeout: 60000,
            browserNoActivityTimeout: 60000,
            singleRun: true,
            proxies: {
                "/" : "http://localhost:8888"
            }
        });
    };
})();
